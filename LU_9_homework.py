class Robot:
    
    def __init__(self):
        self.recordings = []
        
    def listen(self, string):
        self.recordings.append(string)
        
    def play_recordings(self):
        print(self.recordings)
        
    def delete_recordings(self):
        self.recordings = []
        
class FlameThrower(Robot):
    def throw_flames(self):
        print('burn, baby, burn!!!')
        self.delete_recordings()


class CryingRobot(Robot):
    def listen(self,string):
        super().listen(string)
        if len(self.recordings) >= 3:
            print('disk is soooo fulll. Call the wambulance')

class FlyingRobot(Robot):
    def fly(self):
        print("I am flying gracefully in the clouds!")
        if self.recordings:
            self.recordings.pop()

### the if statement above prevents 'popping forever' which
### would return an error. here you just pop if the list
### isnt empty to begin with




print('#### CRYING ROBOT ####')
cr = CryingRobot()
cr.listen('hi')
cr.play_recordings()
print('everything should be fine')
cr.listen('sup')
cr.listen("i'm full")
cr.play_recordings()
print('not fine now')
cr.listen('ill cry')
cr.play_recordings()
print(' ')
print('#### FLYING ROBOT ####')
fr = FlyingRobot()
fr.listen('hello')
fr.play_recordings()
fr.fly()
fr.play_recordings()
fr.listen('hello again')
fr.play_recordings()

print(' ')
print('###### HOMEWORK #######')

class LectureRoom:

    def __init__(self):
        self.capacity = 40
        self.max_capacity = 100
        self.min_capacity = 10

    def valid_capacity(self, new_capacity):
        if new_capacity < self.min_capacity or new_capacity > self.max_capacity:
            print('invalid capacity, must be between',
                  self.min_capacity,'and',self.max_capacity)
            return False
        else:
            return True

    def increase_capacity(self, amount):
        if self.valid_capacity(self.capacity + amount):
            self.capacity += amount

    def decrease_capacity(self, amount):
        if self.valid_capacity(self.capacity - amount):
            self.capacity -= amount

lr = LectureRoom()
lr.increase_capacity(40)
print(lr.capacity)
lr.increase_capacity(40)

class BigRoom(LectureRoom):
    def __init__(self):
        self.capacity = 50
        self.max_capacity = 200
        self.min_capacity = 50
        
class SmallRoom(LectureRoom):
    def __init__(self):
        self.capacity = 10
        self.max_capacity = 20
        self.min_capacity = 5
        
br = BigRoom()
br.increase_capacity(100)
print('big rooms capacity is now',br.capacity)
br.increase_capacity(300)

sr = SmallRoom()
sr.decrease_capacity(5)
print('small rooms capacity is now',sr.capacity)
sr.decrease_capacity(2)

class Tank(FlameThrower):
    def throw_flames(self):
        print('throws VERY LARGE flame')
        self.delete_recordings()
    
    def hulk_stomp(self):
        print('hulk stomping everywhere!')
            
t = Tank()
t.throw_flames()

class LightRobot(Robot):
    def listen(self,string):
        super().listen(string)
        if len(self.recordings) >= 3:
            self.recordings.pop(0)



        
            



